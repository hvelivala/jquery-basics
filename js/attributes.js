
$(document).ready(function(){
  $("button#1").click(function(){
    $("p:first").addClass("intro");
  });
  $("button#2").click(function(){
    $("img").attr("width","500");
  });
  $("button#3").click(function(){
    alert($("p").hasClass("intro"));
  });
  $("button#4").click(function(){
    $("p").html("Hello <b>world!</b>");
  });
  $("button#5").click(function(){
    var $x = $("div");
    $x.prop("color","FF0000");
    $x.append("The color property has the following value: " + $x.prop("color"));
    $x.removeProp("color");
    $x.append("<br>Now the color property has the following value: " + $x.prop("color"));
  });
  $("button#6").click(function(){
    $("p").removeAttr("style");
  });
  $("button#7").click(function(){
    $("p").removeClass("intro");
  });
  $("button#8").click(function(){
    $("p").toggleClass("main");
  });
  $("button#setinput").click(function(){
    $("input:text").val("Glenn Quagmire");
  });
  });
 
