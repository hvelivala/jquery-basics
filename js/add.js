		angular.module('MyApp', ['ngGrid'])
		.controller('MainController', [ '$scope', function($scope) {

			$scope.experience = [1,2,3,4,5,6,7]
			$scope.userType = 'guest';
			$scope.counter = 4;


	    

			$scope.names=[{
									"SerialNo":"1",
									"FirstName":"Bujji",
									"LastName":"Babu",
									"Experience":"4",
									"Project":"MyJourney",
									"SkypeId":"bbalaga@innominds.com",
									"PhoneNo":"9876555553",
									"FromDate":"12/15/2013",
									"ToDate":"12/15/2014"
								},{
									"SerialNo":"2",
									"FirstName":"Sunil",
									"LastName":"Motrue",
									"Experience":"5",
									"Project":"Dbridge",
									"SkypeId":"smotrue@innominds.com",
									"PhoneNo":"9876543211",
									"FromDate":"2/15/2013",
									"ToDate":"12/15/2014"
								},{
									"SerialNo":"3",
									"FirstName":"SaiKumar",
									"LastName":"Desu",
									"Experience":"4",
									"Project":"GBC",
									"SkypeId":"sdesu@innominds.com",
									"PhoneNo":"9657845321",
									"FromDate":"2/15/2013",
									"ToDate":"12/15/2014"
								},{
									"SerialNo":"4",
									"FirstName":"Govardhan",
									"LastName":"Govardhan",
									"Experience":"3",
									"Project":"GBC",
									"SkypeId":"govardhan@innominds.com",
									"PhoneNo":"9765845234",
									"FromDate":"2/15/2013",
									"ToDate":"12/15/2014"
								}];

								
	   
	    $scope.gridOptions = { 
	        data: 'names',
	        showSelectionCheckbox: true,
	        enableCellSelection: true,
	        enableRowSelection: true,
	        enableCellEdit: true,
	        selectedItems: [],
	        columnDefs: [{field: 'SerialNo', displayName: 'Serial No',enableCellEdit: true},{field:'FirstName', displayName:'FirstName',enableCellEdit: true}, {field:'LastName', displayName:'LastName',enableCellEdit: true},{field:'Experience', displayName:'Experience',enableCellEdit: true},{field:'Project', displayName:'Project',enableCellEdit: true},{field:'SkypeId', displayName:'SkypeId',enableCellEdit: true},{field:'PhoneNo', displayName:'PhoneNo',enableCellEdit: true},{field:'FromDate', displayName:'From Date',enableCellEdit: true},{field:'ToDate', displayName:'To Date',enableCellEdit: true}]
	    };
	    $scope.removeRow = function(){
	    	$scope.namesDuplicate=[];
	    	console.log($scope.gridOptions.selectedItems);
            var x=0;

	        angular.forEach($scope.names, function(data, index){
               
               x=0;
	            console.log(data);
	            if(data.name == 'Enos'){
	                $scope.gridOptions.selectItem(index, true);
	            }
	             angular.forEach($scope.gridOptions.selectedItems, function(item, sno){
	             	if(data.SerialNo == item.SerialNo){
                      x=1;
                      
	             	}
	             });
	             if(x==0){
	             	$scope.namesDuplicate.push(data);
	             }
	        });


	        $scope.names=$scope.namesDuplicate;
	    };
		
		$scope.submitForm = function() {
				$scope.counter++;
			    console.log("abc"+$scope.fname);
			    $scope.names.push({
									"SerialNo": $scope.counter,
									"FirstName":$scope.fname,
									"LastName":$scope.lname,
									"Experience":$scope.experienceSelected,
									"Project":$scope.prjct,
									"SkypeId":$scope.skypeid,
									"PhoneNo":$scope.phoneno,
									"FromDate":$scope.fromDate,
									"ToDate":$scope.toDate

								});
		  }
		}]);